const fs = require('fs');

module.exports = savePassword = async (filename) => {
  const passwordData = await fs.promises.readFile('./userFiles/password.json', 'utf-8');
  const state = JSON.parse(passwordData);
  const fileHasPassword = state.findIndex((elem) => elem.filename === filename);
  const newState = state.splice(fileHasPassword);
  const newPasswordData = JSON.stringify(newState);
  await fs.promises.writeFile('./userFiles/password.json', newPasswordData, 'utf-8');
};
