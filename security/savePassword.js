const fs = require('fs');

module.exports = savePassword = async (filename, password) => {
  const passwordData = await fs.promises.readFile('./userFiles/password.json', 'utf-8');
  const state = JSON.parse(passwordData);
  state.push({filename, password});
  const newPasswordData = JSON.stringify(state);
  await fs.promises.writeFile('./userFiles/password.json', newPasswordData, 'utf-8');
};
