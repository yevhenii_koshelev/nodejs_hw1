const fs = require('fs');

module.exports = checkPassword = (filename, password = undefined) => {
  const passwordData = fs.readFileSync('./userFiles/password.json', 'utf-8');
  const state = JSON.parse(passwordData);
  const fileHasPassword = state.find((elem) => elem.filename === filename);

  return fileHasPassword === undefined || password === fileHasPassword.password;
};
