const express = require('express');
const morgan = require('morgan');
const app = express();

const fileRouter = require('./routers/fileRouter');
const checkFileDirectory = require('./middleware/checkFileDirectory');
const checkPasswordFile = require('./middleware/checkPasswordFile');

const PORT = 8080;


app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', checkFileDirectory, checkPasswordFile, fileRouter);


app.listen(PORT);
