const fs = require('fs');
const deletePassword = require('../security/deletePassword');

module.exports = deleteFile = async (req, res) => {
  const filename = req.params.filename;

  try {
    await deletePassword(filename);
    await fs.promises.unlink(`./userFiles/files/${filename}`);
    res.status(200).json({message: `File ${filename} has been deleted`});
  } catch {
    res.status(500).json({message: `Server error`});
  }
};
