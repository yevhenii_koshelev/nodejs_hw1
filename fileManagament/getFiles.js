const fs = require('fs/promises');

module.exports = getFiles = async (req, res) => {
  try {
    const files = await fs.readdir('./userFiles/files');
    res.status(200).json({
      message: 'Success',
      files,
    });
  } catch {
    res.status(500).json({message: 'Server error'});
  }
};
