const fs = require('fs');

module.exports = updateFile = async (req, res) => {
  const filename = req.params.filename;
  const content = req.body.content;

  try {
    await fs.promises.writeFile(`./userFiles/files/${filename}`, `${content}`, 'utf-8');
    res.status(200).json({message: `File has been updated`});
  } catch {
    res.status(500).json({message: `Server error`});
  }
};
