const fs = require('fs');

module.exports = getFile = async (req, res) => {
  const filename = req.params.filename;

  try {
    const content = await fs.promises.readFile(`./userFiles/files/${filename}`, 'utf-8');
    const extension = filename.split('.')[filename.split('.').length - 1];
    const uploadedDate = (await fs.promises.stat(`./userFiles/files/${filename}`)).birthtime;
    res.status(200).json({
      message: 'Success',
      filename,
      content,
      extension,
      uploadedDate,
    });
  } catch {
    res.status(500).json({message: 'Server error'});
  }
};
