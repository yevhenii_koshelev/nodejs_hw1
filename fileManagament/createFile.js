const fs = require('fs/promises');
const savePassword = require('../security/savePassword');

module.exports = createFile = async (req, res) => {
  const password = req.body.password;
  try {
    if (password) {
      await savePassword(req.body.filename, req.body.password);
    }
    await fs.writeFile(`./userFiles/files/${req.body.filename}`, `${req.body.content}`);
    res.status(200).json({message: `File created successfully`});
  } catch {
    res.status(500).json({message: 'Server error'});
  }
};
