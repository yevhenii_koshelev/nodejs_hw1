const path = require('path');
const fs = require('fs');

module.exports = fileValidation = (req, res, next) => {
  const validFileExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
  const filename = req.body.filename;
  const content = req.body.content;

  const filenameLengthIsZero = !filename.trim().length;
  if (filenameLengthIsZero) {
    return res.status(400).json({message: `Invalid file name, please specify 'filename' parameter`});
  }
  const fileExtensionIsValid = validFileExtensions.includes(path.extname(filename));
  if (!fileExtensionIsValid) {
    return res.status(400).json({message: `Invalid file extension, please specify file extension`});
  }
  const contentLengthIsZero = !content.trim().length;
  if (contentLengthIsZero) {
    return res.status(400).json({message: `Invalid content, please specify 'content' parameter`});
  }
  const fileIsExist = fs.readdirSync(`./userFiles/files`).includes(filename);
  if (fileIsExist) {
    return res.status(400).json({message: `File already exists`});
  }
  if (filename === 'passwords.json') {
    return res.status(400).json({message: `Reserved filename, please specify another 'filename' parameter`});
  }

  next();
};
