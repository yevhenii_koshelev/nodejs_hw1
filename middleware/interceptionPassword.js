const checkPassword = require('../security/checkPassword');

module.exports = interceptionPassword = async (req, res, next) => {
  const filename = req.params.filename;
  const passAccess = await checkPassword(filename, req.query.password);

  if (passAccess) {
    next();
  } else {
    res.status(400).json({message: 'Wrong file password'});
  }
};

