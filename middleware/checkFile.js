const fs = require('fs');

module.exports = checkFile = (req, res, next) => {
  const filename = req.params.filename;
  const fileIsExist = !fs.readdirSync(`./userFiles/files`).includes(filename);
  if (fileIsExist) {
    return res.status(400).json({message: `No file with filename '${filename}' found`});
  }
  next();
};
