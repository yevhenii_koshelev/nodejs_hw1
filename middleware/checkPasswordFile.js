const fs = require('fs');

module.exports = checkPasswordFile = async (req, res, next) => {
  const UserFilesPasswordsDirIsNotExist = !fs.readdirSync('./userFiles').includes('password.json');
  if (UserFilesPasswordsDirIsNotExist) {
    try {
      await fs.promises.writeFile('./userFiles/password.json', '[]', 'utf-8');
    } catch {
      return res.status(500).json({message: 'Server error'});
    }
  }

  next();
};
