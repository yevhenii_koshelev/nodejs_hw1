const fs = require('fs');

module.exports = checkFileDirectory = async (req, res, next) => {
  const UserFilesDirIsNotExist = !fs.existsSync('./userFiles');
  if (UserFilesDirIsNotExist) {
    try {
      await fs.promises.mkdir('./userFiles');
    } catch {
      return res.status(500).json({message: 'Server error'});
    }
  }
  const UserFilesFilesDirIsNotExist = !fs.existsSync('./userFiles/files');
  if (UserFilesFilesDirIsNotExist) {
    try {
      await fs.promises.mkdir('./userFiles/files');
    } catch {
      return res.status(500).json({message: 'Server error'});
    }
  }

  next();
};
