const router = require('express').Router();
const createFile = require('../fileManagament/createFile');
const getFiles = require('../fileManagament/getFiles');
const getFile = require('../fileManagament/getFile');
const updateFile = require('../fileManagament/updatefile');
const deleteFile = require('../fileManagament/deleteFile');
const interceptionPassword = require('../middleware/interceptionPassword');
const fileValidation = require('../middleware/fileValidation');
const checkFile = require('../middleware/checkFile');

router.route('/').get(getFiles);
router.route('/').post(fileValidation, createFile);
router.route('/:filename').get(checkFile, interceptionPassword, getFile);
router.route('/:filename').put(checkFile, interceptionPassword, updateFile);
router.route('/:filename').delete(checkFile, interceptionPassword, deleteFile);

module.exports = router;
